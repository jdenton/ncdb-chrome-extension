var search = {};

search.do_search = function()
{
  app.button_helper();
  $('.spinner').show();
  
  var query = $('#site-search').val();
  var search_table = $('#content-search');  
  var found_results = false;
  search_table.html('');
  
  // Search the library	
	var url = site_url + '/api/v1/library/search?search=' + query + '&format=json';
	$.get(url, function (json) {
    if ( json.data.length > 0 )
    {
      search_table.append('<tr><td class="header">NCDB Library</td></tr>');
      $.each( json.data, function( index, r ) {
        var link = r.LibraryLink;
        if ( link === '' )
        {
          link = site_url + 'library/page/' + r.LibraryId;
        }  
        var row = '<tr class="row-library"><td><a href="' + link + '" title="' + r.LibraryTitle + '">' + r.LibraryTitle + '</a>';
            row += ' <span class="muted"><small>in ' + r.LibraryCategories[0].LibraryCategoriesTitle + '</small></span>';
            row += '</td></td>';
        search_table.append( row );
      });
    }  
    
    
    $('.row-library a').on('click', function() {
      app.link_helper( $(this) );
    });
    
    found_results = true;
	});
  
  // Library Selected Topics data
  var url = site_url + '/api/v1/library/search_selected_topics?search=' + query;
	$.get(url, function (json) {
    if ( json.data.length > 0 )
    {
      search_table.append('<tr><td class="header">Selected Topics</td></tr>');
      $.each( json.data, function( index, r ) {
        search_table.append('<tr class="row-categories"><td>' + r.ParentCategory + ' > <a href="' + site_url + '/library/list/' + r.LibraryCategoriesId + '">' + r.LibraryCategoriesTitle + '</a></td></tr>');
      });
      
      $('.row-categories a').on('click', function() {
        app.link_helper( $(this) );
      });
      
      found_results = true;
    }  
	});
  
  // Get wiki data
	var url = site_url + '/api/v1/wiki/search?search=' + query + '&sitesearch=1';
	$.get(url, function (json) {
    if ( json.data.length > 0 )
    {
      search_table.append('<tr><td class="header">Wikis</td></tr>');
      $.each( json.data, function( index, r ) {
        var row = '<tr class="row-wiki"><td>';
            row += '<a href="' + site_url + '/wiki/page/' + r.WikiGroupId + '/' + r.WikiId + '" title="' + r.WikiTitle + '">' + r.WikiTitle + '</a>';
            row += ' <span class="muted"><small> in <a href="' + site_url + '/groups/page/' + r.GroupsId + '">' + r.GroupsTitle + '</a></small></span>';
            row += '</td></tr>';
        search_table.append( row );
      });
      
      $('.row-wiki a').on('click', function() {
        app.link_helper( $(this) );
      });
      
      found_results = true;
    }  
	});
  
  // Get event data
	var url = site_url + '/api/v1/events/search?search=' + query;
	$.get(url, function (json) {
    if ( json.data.length > 0 )
    {
      search_table.append('<tr><td class="header">Events</td></tr>');
      $.each( json.data, function( index, r ) {
        var row = '<tr class="row-event"><td>';
            row += '<a href="' + site_url + '/events/detail/' + r.EventsGroupId + '/' + r.EventsId + '" title="' + r.EventsTitle + '">' + r.EventsTitle + '</a>';
            row += ' <span class="muted"><small>' + r.EventsStart + ' to ' + r.EventsEnd + '</small></span>';
            row += '</td></tr>';
        search_table.append( row );
      });
      
      $('.row-event a').on('click', function() {
        app.link_helper( $(this) );
      });
      
      found_results = true;
    }  
	});
  
  // Get forum data
	var url = site_url + '/api/v1/forum/search?search=' + query;
	$.get(url, function (json) {
    if ( json.data.length > 0 )
    {
      search_table.append('<tr><td class="header">Forums</td></tr>');
      $.each( json.data, function( index, r ) {
        var row = '<tr class="row-forum"><td>';
            row += '<a href="' + site_url + '/forum/thread/' + r.ForumId + '" title="' + r.ForumTitle + '">' + r.ForumTitle + '</a>';
            row += ' <span class="muted"><small> in <a href="' + site_url + '/groups/page/' + r.GroupsId + '">' + r.GroupsTitle + '</a></small></span>';
            row += '</td></tr>';
        search_table.append( row );
      });
      
      $('.row-forum a').on('click', function() {
        app.link_helper( $(this) );
      });
      
      found_results = true;
    }    
	});
  
  // Get connection data
  var url = site_url + '/api/v1/users/search?search=' + query;
	$.get(url, function (json) {
    if ( json.data.length > 0 )
    {
      search_table.append('<tr><td class="header">Connections</td></tr>');
      $.each( json.data, function( index, r ) {
        var user_image = r.UsersImage48x48;
        if ( user_image === '/assets/images/avatarDefault48.png' )
        {
          user_image = site_url + user_image;
        }
        var row = '<tr class="row-connection"><td>';
            row += '<a href="' + site_url + '/members/profile/' + r.UsersId + '" title="View ' + r.UsersFirstName + ' ' + r.UsersLastName + '"><img src="' + user_image + '" class="avatar48 pull-left mR12" /></a>';
            row += '<a href="' + site_url + '/members/profile/' + r.UsersId + '" title="View ' + r.UsersFirstName + ' ' + r.UsersLastName + '"><strong>' + r.UsersFirstName + ' ' + r.UsersLastName + '</strong></a><br />';
            row += r.UsersAgency + ': ' + r.UsersTitle + '<br />';
            row += '<a href="mailto:' + r.UsersEmail + '">' + r.UsersEmail + '</a> ' + r.UsersPhone;
            row += '</td></tr>';
        search_table.append( row );
      });
      
      $('.row-connection a').on('click', function() {
        app.link_helper( $(this) );
      });
      
      found_results = true;
    }  
	});
  
  // Get pages data
  var url = site_url + '/api/v1/pages/search?search=' + query;
	$.get(url, function (json) {
    if ( json.data.length > 0 )
    {
      search_table.append('<tr><td class="header">Site Pages</td></tr>');
      $.each( json.data, function( index, r ) {
        var row = '<tr class="row-page"><td>';
            row += '<a href="' + r.Url + '" title="' + r.PagesTitle + '">' + r.PagesTitle + '</a>';
            row += ' <span class="muted"><small> updated on ' + r.Date + '</small></span>';
            row += '</td></tr>';
        search_table.append( row );
      });
      
      $('.row-page a').on('click', function() {
        app.link_helper( $(this) );
      });
      
      found_results = true;
    }  
	});	
  
  // Get Families Lead data
  var url = site_url + '/api/v1/remotesearch/search?search=' + query + '&site=familieslead';
	$.get(url, function (json) {
    if (json.data.length > 0) {
      search_table.append('<tr><td class="header">Families Lead Website</td></tr>');
      $.each( json.data, function( index, r ) {
        search_table.append('<tr class="row-familieslead"><td><img src="../images/icon-families-lead.png" class="pull-left mR12 avatar32" /><a href="' + r.cPath + '">' + r.cName + '</a><br />' + r.content + '</td></tr>');
      });
      
      $('.row-familieslead a').on('click', function() {
        app.link_helper( $(this) );
      });
     
      found_results = true;
    }
	});
  
  // Get Literacy data
  var url = site_url + '/api/v1/remotesearch/search?search=' + query + '&site=literacy';
	$.get(url, function (json) {
    if (json.data.length > 0) {
      search_table.append('<tr><td class="header">Literacy Website</td></tr>');
      $.each( json.data, function( index, r ) {
        search_table.append('<tr class="row-literacy"><td><img src="../images/icon48.png" class="pull-left mR12 avatar32" /><a href="' + r.cPath + '">' + r.cName + '</a><br />' + r.content + '</td></tr>');
      });
      
      $('.row-literacy a').on('click', function() {
        app.link_helper( $(this) );
      });
      
      found_results = true;
    }
    $('.spinner').hide();
	});
}