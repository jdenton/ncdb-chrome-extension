var app = {};
app.groups = null;
app.bookmarks = null;

var site_url = 'https://nationaldb.org';
//var site_url = 'http://nationaldb.dev';
var logged = false;

app.init = function()
{    
  // Set up notifications alarm
  chrome.alarms.create('notifications', { periodInMinutes: 3 })
  
  // See if we're logged in 
  app.check_login();
  
  // Get app launch panel icons
  app.get_launch_panel();
  
  $('#btnLogin').on('click', function() {
    chrome.tabs.create({ url: site_url + '/login' });
  });
  
  $('#btnGroups').on('click', function() {
    app.get_groups();
  });
  
  $('#btnNotifications').on('click', function() {
    app.get_notifications();
  });
  
  $('#btnBookmarks').on('click', function() {
    app.get_bookmarks();
  });
  
  $('#btnSearchNCDB').on('click', function() {
    $('.content-pane').hide();
    $('#content-pane-search').fadeIn('fast');
    search.do_search();
    
    return false;
  });
  
  // Turn on app panel
  $('#content-pane-apps').fadeIn('fast');
  app.button_helper( $('#btnApps') );
  
  $('.content-control').on('click', function() {
    var $this = $(this);
    app.button_helper( $this );
    var pane = $this.data('pane');
    $('.content-pane').hide();
    $('#content-pane-' + pane).fadeIn('fast');    
  });
}

app.check_login = function()
{
  var url = site_url + '/api/v1/users/check_login';
	$.get(url, function (json) {
    if ( json.data !== 'false' )
    {
      $('.logged-in').show();
      $('.logged-out').hide();
      logged = true;
    } else {
      $('.logged-in').hide();
      $('.logged-out').show();
    }
	});
}

app.get_groups = function()
{
  if ( app.groups === null )
  {  
    $('.spinner').show();
    var url = site_url + '/api/v1/groups/my_groups';
    $.get(url, function (json) {
      $('.spinner').hide();
      app.groups = json.data;
      $.each( app.groups, function( index, group ) {
        var group_image = group.GroupsImage48x48;
        if ( group_image === '/assets/images/avatarDefaultGroup48.png' )
        {
          group_image = site_url + group_image;
        } 
        var row = '<tr><td>';
            row += '<a href="' + site_url + '/groups/page/' + group.GroupsId + '" title="Visit the ' + group.GroupsTitle + ' group"><img src="' + group_image + '" class="avatar48" /></a>';
            row += '</td><td><a href="' + site_url + '/groups/page/' + group.GroupsId + '">' + group.GroupsTitle + '</a></td></tr>';
        $('#content-groups').append( row );
      });
      
      $('#content-groups a').on('click', function() {
        var $this = $(this);
        var url = $this.attr('href');
        chrome.tabs.create({ url: url });
      });
    });
  }  
}

app.get_notifications = function()
{
  var url = site_url + '/api/v1/notifications/for_me';
  $('.spinner').show();
  $.get(url, function (json) {
    $('.spinner').hide();
    $.each( json.data, function( index, notification ) {
      var user_image = notification.UsersImage48x48;
      if ( user_image === '/assets/images/avatarDefault48.png' || user_image === '/assets/images/avatarDefaultNCDB48-new.png' )
      {
        user_image = site_url + user_image;
      }
      var row = '<tr><td style="min-width: 48px;"><img src="' + user_image + '" class="avatar48" /></td>'
          row += '<td class="span6">' + notification.NotificationsTitle + '<br />';
          row += '<span class="pull-right muted"><small>' + notification.Date + ' in <a href="' + site_url + '/groups/page/' + notification.NotificationsGroupId + '">' + notification.NotificationsGroupName + '</a></small></span></td></tr>';
      $('#content-notifications').append( row );
    });
    
    $('#content-notifications a').on('click', function() {
        app.link_helper( $(this) );
      });
  });
}

app.get_bookmarks = function()
{
  var url = site_url + '/api/v1/bookmarks/for_me';
  $('.spinner').show();
  $.get(url, function (json) {
    $('.spinner').hide();
    $.each( json.data, function( index, bookmark ) {
      var row = '<tr><td>';
          row += '<a href="' + bookmark.Url + '" title="' + bookmark.Title + '">'
          row += bookmark.Title + '</a> <span class="muted">in ' + bookmark.BookmarkCatsTitle + '</span></td></tr>';
      $('#content-bookmarks').append( row );
    });
    
    $('#content-bookmarks a').on('click', function() {
      app.link_helper( $(this) );
    });
  });
}

app.get_launch_panel = function()
{
  var url = site_url + '/api/v1/app/app_launch_panel';
  $.get(url, function (json) {
    $.each( json.data, function( index, app ) {
      var row = '<div><a href="' + app.url + '" title="' + app.name + '"><img src="' + app.icon + '" /><br />';
          row += app.name + '</a></div>';
      $('#content-pane-apps').append( row );
    });
    
    $('#content-pane-apps a').on('click', function() {
      app.link_helper( $(this) );
    });
  });
}

app.link_helper = function( lnk_obj )
{
  var url = lnk_obj.attr('href');
  chrome.tabs.create({ url: url + '?chrome' });
}

app.button_helper = function( btn_obj )
{
  $('.content-control').removeClass('disabled');
  if ( typeof btn_obj === 'object' )
  {  
    btn_obj.addClass('disabled');
  }  
}

$(document).ready(function() {
  app.init();
});

